class CreateMemoirs < ActiveRecord::Migration[6.0]
  def change
    create_table :memoirs do |t|
      t.string :title
      t.text :description
      t.text :summary

      t.timestamps
    end
  end
end
