class AddMemoirToAudios < ActiveRecord::Migration[6.0]
  def change
    add_reference :audios, :memoir, foreign_key: true
  end
end
