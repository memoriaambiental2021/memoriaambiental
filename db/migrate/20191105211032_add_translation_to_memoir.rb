class AddTranslationToMemoir < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        Memoir.create_translation_table! :title => :string, :summary => :text, :description => :text, :report => :text
      end

      dir.down do
        Memoir.drop_translation_table!
      end
    end
  end
end
