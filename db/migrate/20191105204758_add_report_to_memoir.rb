class AddReportToMemoir < ActiveRecord::Migration[6.0]
  def change
    add_column :memoirs, :report, :text
  end
end
