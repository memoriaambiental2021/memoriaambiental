class CreateAudios < ActiveRecord::Migration[6.0]
  def change
    create_table :audios do |t|
      t.string :title
      t.string :description
      t.string :url
    end
  end
end
