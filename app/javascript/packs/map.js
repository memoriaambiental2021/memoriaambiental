console.log('hola mapa');
loadMap();

function loadMap() {

  mapboxgl.accessToken = 'pk.eyJ1IjoiZGFxdWlyb3pmY2kiLCJhIjoiY2p2ODJ6eTBsMG1kejN5b2p3cjQ5amlocyJ9.Vax2BkFD0rhM8LjTdecD9w';
  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/daquirozfci/cjv830nyz1bah1gmpg5iwpkdh',
    zoom: 7.00,
    center: [-84.216, 9.890],
  });

  var geojson = {
    type: 'FeatureCollection',
    features: [{
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [-85.418, 10.058]
      },
      properties: {
        title: 'Hojancha',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
      }
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [-84.300, 10.890]
      },
      properties: {
        title: 'Mapbox',
        description: 'San Francisco, California'
      }
    }]
  };

  // add markers to map
  geojson.features.forEach(function(marker) {
    // create a HTML element for each feature
    var el = document.createElement('div');
    el.className = 'marker';

    // make a marker for each feature and add to the map
    new mapboxgl.Marker(el)
      .setLngLat(marker.geometry.coordinates)
      .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
      .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
      .addTo(map);
  });

}
