class Resource < ApplicationRecord
  translates :title, :description
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_one_attached :attach
  belongs_to :memoir

end
