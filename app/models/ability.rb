class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, Memoir, public: true

    if user.present?
      can [:read, :create], Memoir
      can [:update, :destroy], Memoir, :user_id => user.id
      if user.has_role? :admin
        can :manage, :all
        can :dashboard, :all
        can :access, :rails_admin
      elsif user.has_role? :editor
        can :manage, :all
        can :dashboard, :all
        can :access, :rails_admin
      end
    end

  end
end
