class Memoir < ApplicationRecord
  translates :title, :description, :summary, :report
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_rich_text :post

  has_one_attached :image
  has_many_attached :gallery

  belongs_to :area

  belongs_to :user

  has_many :resources
  accepts_nested_attributes_for :resources

  has_many :audios
  accepts_nested_attributes_for :audios

  has_many :quotes
  accepts_nested_attributes_for :quotes

end
