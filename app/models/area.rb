class Area < ApplicationRecord
  translates :name
  accepts_nested_attributes_for :translations, allow_destroy: true

  # validates :latitude, presence: true
  # validates :longitude, presence: true
  #
  has_many :memoirs

  geocoded_by :name
  # can be used to retrieve coordinates based on the provided address
  # after_validation :geocode, if: ->(obj){ obj.name.present? and obj.name_changed? }
  after_validation :geocode, if: ->(obj){ obj.name.present? and obj.name_changed? }

end
